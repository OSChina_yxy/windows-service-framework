﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsServiceFramework.Core.Configuration;
using WindowsServiceFramework.Core.ServiceTask;

namespace WindowsServiceFramework
{
    public class TestTask : BaseServiceTask, IServiceTask
    {
        private IConfiguration _Configuration;

        public TestTask(IConfiguration Configuration)
        {
            _Configuration = Configuration;
            string intervalText = _Configuration.GetSection("Interval").Value;
            if (int.TryParse(intervalText, out int interval))
            {
                base.Interval = interval;
            }
        }

        protected override void Do()
        {
            Console.WriteLine("Test");
        }
    }
}
