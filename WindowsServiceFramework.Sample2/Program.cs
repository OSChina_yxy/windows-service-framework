﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using WindowsServiceFramework.Core.Configuration;
using WindowsServiceFramework.Core.ServiceBuilder;

namespace WindowsServiceFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                CreateDefaultServiceBuilder(args)
                    .RegisterServiceTask(typeof(TestTask))
                    .ConfigureServiceOptions((config, option) =>
                    {
                        option.ServiceName = config.GetSection("ServiceName").Value;
                        option.DisplayName = config.GetSection("DisplayName").Value;
                        option.Description = config.GetSection("ServiceDescription").Value;
                    })
                    //.UseStartup<Startup>()
                    .Build()
                    //.Run() //Debug Run As Console
                    .RunAsService(); //Run As WindowsService
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Fatal(ex);
            }
        }

        public static IServiceBuilder CreateDefaultServiceBuilder(string[] args)
        {
            return new DefaultServiceBuilder()
                    .RegisterBeforeRun(configuration =>
                    {
                        NLog.LogManager.GetCurrentClassLogger().Info("Service Strat");
                    })
                    .RegisterAfterStop(configuration =>
                    {
                        NLog.LogManager.GetCurrentClassLogger().Info("Service End");
                    });
        }
    }
}
