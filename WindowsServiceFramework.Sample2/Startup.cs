﻿
using System;
using System.Linq;
using System.Reflection;
using WindowsServiceFramework.Core;
using WindowsServiceFramework.Core.Configuration;
using WindowsServiceFramework.Core.ServiceTask;
using WindowsServiceFramework.Core.ServiceTaskIoc;

namespace WindowsServiceFramework
{
    public class Startup : IStartup
    {
        IConfiguration Configuration;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void Configure(ContainerBuilderWarp containerBuilderWarp)
        {
            containerBuilderWarp.RegisterTypeAsSingleInstance<Test, ITest>();
        }
    }

    public interface ITest
    {
        void Say();
    }

    public class Test : ITest
    {
        public void Say()
        {
            Console.WriteLine("wssss");
        }
    }

}