# WindowsServiceFramework

#### 介绍
framework4.0下得windows-service框架

仿 asp.netcore (低配)

#### 软件架构

#### 安装教程

```powershell
install-package WindowsServiceFramework.Core
```

#### 使用说明

> 默认读取 appsettings.json (手动创建)

```C#
//Program.cs
static void Main(string[] args)
        {
            try
            {
                CreateDefaultServiceBuilder(args)
                    .RegisterServiceTask(typeof(TestTask))
                    .ConfigureServiceOptions((config, option) =>
                    {
                        option.ServiceName = config.GetSection("ServiceName").Value;
                        option.DisplayName = config.GetSection("DisplayName").Value;
                        option.Description = config.GetSection("ServiceDescription").Value;
                    })
                    .Build()
                    //.Run();//Debug Run as Console
                    .RunAsService();//Run As WindowsService
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static IServiceBuilder CreateDefaultServiceBuilder(string[] args)
        {
            return new DefaultServiceBuilder()
                    .RegisterBeforeRun(configuration =>
                    {
                        Console.WriteLine("Service Start");
                    })
                    .RegisterAfterStop(configuration =>
                    {
                        Console.WriteLine("Service End");
                    });
        }
```

```C#
//The BaseServiceTask's Default Interval = 1000 (ms)
public class TestTask : BaseServiceTask, IServiceTask
    {
        protected override void Do()
        {
            Console.WriteLine("Test");
        }
    }
```







