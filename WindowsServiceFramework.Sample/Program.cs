﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsServiceFramework.Core.ServiceBuilder;

namespace WindowsServiceFramework.Sample
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                CreateDefaultServiceBuilder(args)
                    .RegisterServiceTask(typeof(TestTask))
                    .ConfigureServiceOptions((config, option) =>
                    {
                        option.ServiceName = config.GetSection("ServiceName").Value;
                        option.DisplayName = config.GetSection("DisplayName").Value;
                        option.Description = config.GetSection("ServiceDescription").Value;
                    })
                    .Build()
                    .Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static IServiceBuilder CreateDefaultServiceBuilder(string[] args)
        {
            return new DefaultServiceBuilder()
                    .RegisterBeforeRun(configuration =>
                    {
                        Console.WriteLine("Service Start");
                    })
                    .RegisterAfterStop(configuration =>
                    {
                        Console.WriteLine("Service End");
                    });
        }
    }
}
