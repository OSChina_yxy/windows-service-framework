﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsServiceFramework.Core.Configuration;
using WindowsServiceFramework.Core.ServiceTask;

namespace WindowsServiceFramework
{
    public class TestTask : BaseServiceTask, IServiceTask
    {
        protected override void Do()
        {
            Console.WriteLine("Test");
        }
    }
}
