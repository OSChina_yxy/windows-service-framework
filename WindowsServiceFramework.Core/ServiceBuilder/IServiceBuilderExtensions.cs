﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.ServiceBuilder
{
    public static class IServiceBuilderExtensions
    {
        public static IServiceBuilder UseStartup<T>(this IServiceBuilder serviceBuilder) where T : IStartup
        {
            return serviceBuilder.UseStartup(typeof(T));
        }
    }
}
