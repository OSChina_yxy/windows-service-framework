﻿using Autofac;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using WindowsServiceFramework.Core.Configuration;
using WindowsServiceFramework.Core.Service;
using WindowsServiceFramework.Core.ServiceScheduler;
using WindowsServiceFramework.Core.ServiceTask;

namespace WindowsServiceFramework.Core.ServiceBuilder
{
    public class DefaultServiceBuilder : IServiceBuilder
    {
        private ServiceOption _serviceOption;
        private ContainerBuilder _applicationContainerBuilder;
        private IContainer _container;
        private IConfiguration _configuration;
        private IConfigurationBuilder _configurationBuilder;
        IList<Type> types = new List<Type>();
        private IList<Action<IConfiguration>> _beforeRunActions = new List<Action<IConfiguration>>();

        private IList<Action<IConfiguration>> _afterStopActions = new List<Action<IConfiguration>>();

        private Action<IConfiguration, ServiceOption> _configureServiceOptionsAction;

        private bool _useStartup = false;

        public DefaultServiceBuilder()
        {
            _serviceOption = new ServiceOption();
            _applicationContainerBuilder = new ContainerBuilder();
            _configurationBuilder = new ConfigurationBuilder();
        }

        public IServiceBuilder ConfigureServiceOptions(Action<IConfiguration, ServiceOption> action)
        {
            _configureServiceOptionsAction = action;
            return this;
        }

        public IService Build()
        {
            RegisterDefaultApp();
            _container = _applicationContainerBuilder.Build();
            ContainerBuilder containerBuilder = new ContainerBuilder();
            ConfigureStartup(containerBuilder);
            return new TopshelfService(_serviceOption, containerBuilder.Build(), this._beforeRunActions, this._afterStopActions);
        }

        public IServiceBuilder RegisterServiceTask(Type serviceTaskType)
        {
            types.Add(serviceTaskType);
            return this;
        }

        public IServiceBuilder UseStartup(Type startupType)
        {
            _applicationContainerBuilder.RegisterType(startupType).As<IStartup>().SingleInstance();
            _useStartup = true;
            return this;
        }

        void ConfigureStartup(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterInstance(_configurationBuilder).As<IConfigurationBuilder>().SingleInstance();

            containerBuilder.RegisterInstance(_configuration).As<IConfiguration>().SingleInstance();

            containerBuilder.RegisterType<DefaultServiceScheduler>().As<IServiceScheduler>().SingleInstance();

            if (_useStartup)
            {
                IStartup startup = _container.Resolve<IStartup>();

                startup?.Configure(containerBuilder);
            }

            foreach (var serviceTaskType in types)
            {
                containerBuilder.RegisterType(serviceTaskType).Named<IServiceTask>(serviceTaskType.FullName);
            }
        }

        void RegisterDefaultApp()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "appsettings.json");
            if (File.Exists(path))
            {
                _configurationBuilder.AddJsonFile(path);
            }
            _configuration = _configurationBuilder.Build();
            _configureServiceOptionsAction?.Invoke(this._configuration, this._serviceOption);
            _applicationContainerBuilder.RegisterInstance(_configurationBuilder).As<IConfigurationBuilder>().SingleInstance();
            _applicationContainerBuilder.RegisterInstance(_configuration).As<IConfiguration>().SingleInstance();

        }

        public IServiceBuilder ConfigureConfiguration(Action<IConfigurationBuilder> action)
        {
            action?.Invoke(_configurationBuilder);
            return this;
        }

        public IServiceBuilder RegisterBeforeRun(Action<IConfiguration> action)
        {
            this._beforeRunActions.Add(action);
            return this;
        }

        public IServiceBuilder RegisterAfterStop(Action<IConfiguration> action)
        {
            this._afterStopActions.Add(action);
            return this;
        }
    }
}
