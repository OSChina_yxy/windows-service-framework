﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsServiceFramework.Core.Configuration;
using WindowsServiceFramework.Core.Service;
using WindowsServiceFramework.Core.ServiceTask;

namespace WindowsServiceFramework.Core.ServiceBuilder
{
    public interface IServiceBuilder
    {
        IService Build();

        IServiceBuilder RegisterServiceTask(Type serviceTaskType);

        IServiceBuilder UseStartup(Type startupType);

        IServiceBuilder ConfigureServiceOptions(Action<IConfiguration,ServiceOption> action);

        IServiceBuilder ConfigureConfiguration(Action<IConfigurationBuilder> action);

        IServiceBuilder RegisterBeforeRun(Action<IConfiguration> action);

        IServiceBuilder RegisterAfterStop(Action<IConfiguration> action);

    }
}
