﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsServiceFramework.Core;
using WindowsServiceFramework.Core.ServiceTask;

namespace WindowsServiceFramework.Core
{
    public interface IStartup
    {
        void Configure(ContainerBuilder containerBuilder);
    }

    public static class ContainerBuilderExtensions
    {
        public static ContainerBuilder RegisterTask(this ContainerBuilder containerBuilder, Type serviceTaskType)
        {
            containerBuilder.RegisterType(serviceTaskType).Named<IServiceTask>(serviceTaskType.FullName);
            return containerBuilder;
        }

        public static ContainerBuilder RegisterTask<T>(this ContainerBuilder containerBuilder) where T : IServiceTask, new()
        {
            containerBuilder.RegisterTask(typeof(T));
            return containerBuilder;
        }
    }
}
