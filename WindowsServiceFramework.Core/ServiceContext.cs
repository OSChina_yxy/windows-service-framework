﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core
{
    public class ServiceContext
    {
        public IDictionary<string, object> Items { get; set; }
    }
}
