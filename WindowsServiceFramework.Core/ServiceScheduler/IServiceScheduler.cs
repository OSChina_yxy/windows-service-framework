﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsServiceFramework.Core.ServiceTask;

namespace WindowsServiceFramework.Core.ServiceScheduler
{
    public interface IServiceScheduler<TServiceTask> where TServiceTask : IServiceTask
    {
        void RegisterTask(TServiceTask serviceTask);
        void Start();
        void Stop();
    }

    public interface IServiceScheduler : IServiceScheduler<IServiceTask>
    {

    }
}
