﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using WindowsServiceFramework.Core.ServiceTask;

namespace WindowsServiceFramework.Core.ServiceScheduler
{
    public class DefaultServiceScheduler : IServiceScheduler
    {
        private IList<IServiceTask> _serviceTasks = new List<IServiceTask>();

        public void RegisterTask(IServiceTask _serviceTask)
        {
            if (_serviceTasks.Contains(_serviceTask))
            {
                throw new ApplicationException("已经注册了服务：" + _serviceTask.GetType().Name);
            }
            _serviceTasks.Add(_serviceTask);
        }

        public void Start()
        {
            foreach (var serviceTask in _serviceTasks)
            {
                serviceTask.Start();
            }
        }

        public void Stop()
        {
            foreach (var service in _serviceTasks)
            {
                service.Stop();
            }

            while (!_serviceTasks.ToList().TrueForAll(p => p.IsComplete()))
            {
                Thread.Sleep(100);
            }
        }
    }
}
