﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.ServiceTask
{
    public interface IServiceTask
    {
        void Start();

        void Stop();

        bool IsComplete();
    }
}
