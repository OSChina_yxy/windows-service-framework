﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WindowsServiceFramework.Core.ServiceTask
{
    public abstract class BaseServiceTask : IServiceTask
    {
        private bool complete;
        private bool runing;
        protected int Interval;

        protected EventHandler<ErrorArgs> OnNoCatchedErrorHappen;

        public BaseServiceTask()
        {
            complete = false;
            runing = false;
            Interval = 1000;
        }

        public bool IsComplete()
        {
            return complete;
        }

        public void Start()
        {
            runing = true;
            Task.Factory.StartNew(() =>
            {
                while (runing)
                {
                    try
                    {
                        complete = false;
                        Do();
                    }
                    catch (Exception ex)
                    {
                        OnNoCatchedErrorHappen?.Invoke(this, new ErrorArgs(ex));
                    }
                    finally
                    {
                        complete = true;
                    }
                    Thread.Sleep(Interval);
                }

            });
        }

        protected virtual void Do()
        {

        }

        public void Stop()
        {
            runing = false;
        }
    }

    public class ErrorArgs : EventArgs
    {
        public ErrorArgs(Exception ex)
        {
            this.Ex = ex;
        }

        public Exception Ex;
    }
}
