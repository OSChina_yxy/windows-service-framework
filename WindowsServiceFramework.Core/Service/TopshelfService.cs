﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Topshelf;
using WindowsServiceFramework.Core.Configuration;
using WindowsServiceFramework.Core.ServiceScheduler;
using WindowsServiceFramework.Core.ServiceTask;

namespace WindowsServiceFramework.Core.Service
{
    public class TopshelfService : IService
    {
        private IServiceScheduler _serviceScheduler;

        private ServiceOption _serviceOption;

        private IContainer _container;

        private IList<Action<IConfiguration>> _beforeRunActions;

        private IList<Action<IConfiguration>> _afterStopActions;

        private IConfiguration _configuration;

        public TopshelfService(ServiceOption serviceOption, IContainer container, IList<Action<IConfiguration>> beforeRunActions, IList<Action<IConfiguration>> afterStopActions)
        {
            this._serviceOption = serviceOption;
            this._container = container;
            this._serviceScheduler = _container.Resolve<IServiceScheduler>();
            this._beforeRunActions = beforeRunActions;
            this._afterStopActions = afterStopActions;
            this._configuration = _container.Resolve<IConfiguration>();
            ServiceSchedulerInitialze(container);
        }

        public void ServiceSchedulerInitialze(IContainer container)
        {
            Type type = typeof(IServiceTask);

            Type[] types = Assembly.GetEntryAssembly().GetTypes()
                .Where(t => type.IsAssignableFrom(t) && !t.IsAbstract && !t.IsInterface).ToArray();

            foreach (Type t in types)
            {
                if (container.IsRegisteredWithName<IServiceTask>(t.FullName))
                {
                    IServiceTask serviceTask = container.ResolveNamed<IServiceTask>(t.FullName);
                    this._serviceScheduler.RegisterTask(serviceTask);
                }
            }
        }

        public void Run()
        {
            foreach (var action in this._beforeRunActions)
            {
                action(this._configuration);
            }

            _serviceScheduler.Start();

            Console.ReadKey();

            foreach (var action in this._afterStopActions)
            {
                action(this._configuration);
            }
        }

        public void RunAsService()
        {
            TopshelfExitCode topshelfExitCode = HostFactory.Run(config =>
            {
                config.Service<IServiceScheduler>(sc =>
                {
                    sc.ConstructUsing(sf => this._serviceScheduler);

                    sc.WhenStarted(serviceScheduler =>
                    {
                        foreach (var action in this._beforeRunActions.Reverse())
                        {
                            action(this._configuration);
                        }

                        serviceScheduler.Start();
                    });

                    sc.WhenStopped(serviceScheduler =>
                    {
                        serviceScheduler.Stop();

                        foreach (var action in this._beforeRunActions.Reverse())
                        {
                            action(this._configuration);
                        }
                    });
                });

                config.SetServiceName(_serviceOption.ServiceName);
                config.SetDisplayName(_serviceOption.DisplayName);
                config.SetDescription(_serviceOption.Description);
                config.RunAsLocalSystem();

            });
            var exitCode = (int)Convert.ChangeType(topshelfExitCode, topshelfExitCode.GetTypeCode());
            Environment.ExitCode = exitCode;
        }
    }
}
