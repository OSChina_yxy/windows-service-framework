﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Service
{
    public class ServiceOption
    {
        public string ServiceName { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public int Interval { get; set; }

        public ServiceOption()
        {
            ServiceName = "DefaultServiceName";
            DisplayName = "DefaultDisplayName";
            Description = "DefaultDescription";
        }
    }
}
