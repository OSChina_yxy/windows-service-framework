﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Service
{
    public interface IService
    {
        void Run();

        void RunAsService();
    }
}
