﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
	public interface IConfigurationSection : IConfiguration
	{
		string Key { get; }

		string Path { get; }

		string Value { get; set; }
	}
}
