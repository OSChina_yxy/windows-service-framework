﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
	public static class ConfigurationPath
	{
		public static string Combine(params string[] pathSegments)
		{
			if (pathSegments == null)
			{
				throw new ArgumentNullException("pathSegments");
			}
			return string.Join(ConfigurationPath.KeyDelimiter, pathSegments);
		}
		public static string Combine(IEnumerable<string> pathSegments)
		{
			if (pathSegments == null)
			{
				throw new ArgumentNullException("pathSegments");
			}
			return string.Join(ConfigurationPath.KeyDelimiter, pathSegments);
		}
		public static string GetSectionKey(string path)
		{
			if (string.IsNullOrEmpty(path))
			{
				return path;
			}
			int num = path.LastIndexOf(ConfigurationPath.KeyDelimiter, StringComparison.OrdinalIgnoreCase);
			if (num != -1)
			{
				return path.Substring(num + 1);
			}
			return path;
		}

		public static string GetParentPath(string path)
		{
			if (string.IsNullOrEmpty(path))
			{
				return null;
			}
			int num = path.LastIndexOf(ConfigurationPath.KeyDelimiter, StringComparison.OrdinalIgnoreCase);
			if (num != -1)
			{
				return path.Substring(0, num);
			}
			return null;
		}
		public static readonly string KeyDelimiter = ":";
	}
}
