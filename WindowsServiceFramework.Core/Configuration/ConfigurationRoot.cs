﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
	public class ConfigurationRoot : IConfigurationRoot, IConfiguration, IDisposable
	{
		public ConfigurationRoot(IList<IConfigurationProvider> providers)
		{
			if (providers == null)
			{
				throw new ArgumentNullException("providers");
			}
			this._providers = providers;
			using (IEnumerator<IConfigurationProvider> enumerator = providers.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					IConfigurationProvider p = enumerator.Current;
					p.Load();
				}
			}
		}

		public IEnumerable<IConfigurationProvider> Providers
		{
			get
			{
				return this._providers;
			}
		}

		public string this[string key]
		{
			get
			{
				for (int i = this._providers.Count - 1; i >= 0; i--)
				{
					string result;
					if (this._providers[i].TryGet(key, out result))
					{
						return result;
					}
				}
				return null;
			}
			set
			{
				if (!this._providers.Any<IConfigurationProvider>())
				{
					throw new InvalidOperationException("No Resoures");
				}
				foreach (IConfigurationProvider configurationProvider in this._providers)
				{
					configurationProvider.Set(key, value);
				}
			}
		}
		public IEnumerable<IConfigurationSection> GetChildren()
		{
			return this.GetChildrenImplementation(null);
		}

		public IConfigurationSection GetSection(string key)
		{
			return new ConfigurationSection(this, key);
		}
		public void Dispose()
		{
			foreach (IConfigurationProvider configurationProvider in this._providers)
			{
				IDisposable disposable2 = configurationProvider as IDisposable;
				if (disposable2 != null)
				{
					disposable2.Dispose();
				}
			}
		}

		private readonly IList<IConfigurationProvider> _providers;

	}
}
