﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
	public class ConfigurationBuilder : IConfigurationBuilder
	{
		public IList<IConfigurationSource> Sources { get; } = new List<IConfigurationSource>();

		public IDictionary<string, object> Properties { get; } = new Dictionary<string, object>();

		public IConfigurationBuilder Add(IConfigurationSource source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			this.Sources.Add(source);
			return this;
		}

		public IConfigurationRoot Build()
		{
			List<IConfigurationProvider> list = new List<IConfigurationProvider>();
			foreach (IConfigurationSource configurationSource in this.Sources)
			{
				IConfigurationProvider item = configurationSource.Build(this);
				list.Add(item);
			}
			return new ConfigurationRoot(list);
		}
	}
}
