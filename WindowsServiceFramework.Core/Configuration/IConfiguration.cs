﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
	public interface IConfiguration
	{
		string this[string key]
		{
			get;
			set;
		}
		IConfigurationSection GetSection(string key);

		IEnumerable<IConfigurationSection> GetChildren();

	}
}
