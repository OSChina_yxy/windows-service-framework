﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
	public static class ConfigurationExtensions
	{
		public static IConfigurationBuilder Add<TSource>(this IConfigurationBuilder builder, Action<TSource> configureSource) where TSource : IConfigurationSource, new()
		{
			TSource tsource = Activator.CreateInstance<TSource>();
			if (configureSource != null)
			{
				configureSource(tsource);
			}
			return builder.Add(tsource);
		}

		public static string GetConnectionString(this IConfiguration configuration, string name)
		{
			if (configuration == null)
			{
				return null;
			}
			IConfigurationSection section = configuration.GetSection("ConnectionStrings");
			if (section == null)
			{
				return null;
			}
			return section[name];
		}

		public static IEnumerable<KeyValuePair<string, string>> AsEnumerable(this IConfiguration configuration)
		{
			return configuration.AsEnumerable(false);
		}

		public static IEnumerable<KeyValuePair<string, string>> AsEnumerable(this IConfiguration configuration, bool makePathsRelative)
		{
			Stack<IConfiguration> stack = new Stack<IConfiguration>();
			stack.Push(configuration);
			IConfigurationSection configurationSection = configuration as IConfigurationSection;
			int prefixLength = (makePathsRelative && configurationSection != null) ? (configurationSection.Path.Length + 1) : 0;
			while (stack.Count > 0)
			{
				IConfiguration config = stack.Pop();
				IConfigurationSection configurationSection2 = config as IConfigurationSection;
				if (configurationSection2 != null && (!makePathsRelative || config != configuration))
				{
					yield return new KeyValuePair<string, string>(configurationSection2.Path.Substring(prefixLength), configurationSection2.Value);
				}
				foreach (IConfigurationSection item in config.GetChildren())
				{
					stack.Push(item);
				}
				config = null;
			}
			yield break;
		}

		public static bool Exists(this IConfigurationSection section)
		{
			return section != null && (section.Value != null || section.GetChildren().Any<IConfigurationSection>());
		}
	}
}
