﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
	public class ConfigurationSection : IConfigurationSection, IConfiguration
	{
		public ConfigurationSection(IConfigurationRoot root, string path)
		{
			if (root == null)
			{
				throw new ArgumentNullException("root");
			}
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			this._root = root;
			this._path = path;
		}
		public string Path
		{
			get
			{
				return this._path;
			}
		}
		public string Key
		{
			get
			{
				if (this._key == null)
				{
					this._key = ConfigurationPath.GetSectionKey(this._path);
				}
				return this._key;
			}
		}

		public string Value
		{
			get
			{
				return this._root[this.Path];
			}
			set
			{
				this._root[this.Path] = value;
			}
		}

		public string this[string key]
		{
			get
			{
				return this._root[ConfigurationPath.Combine(new string[]
				{
					this.Path,
					key
				})];
			}
			set
			{
				this._root[ConfigurationPath.Combine(new string[]
				{
					this.Path,
					key
				})] = value;
			}
		}
		public IConfigurationSection GetSection(string key)
		{
			return this._root.GetSection(ConfigurationPath.Combine(new string[]
			{
				this.Path,
				key
			}));
		}
		public IEnumerable<IConfigurationSection> GetChildren()
		{
			return this._root.GetChildrenImplementation(this.Path);
		}

		private readonly IConfigurationRoot _root;

		private readonly string _path;

		private string _key;
	}
}
