﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
    public static class ConfigurationSectionExtensions
    {
        public static T Get<T>(this IConfigurationSection configurationSection)
        {
            string value = configurationSection.Value;
            if (string.IsNullOrEmpty(value))
            {
                return default(T);
            }

            return JsonConvert.DeserializeObject<T>(value);
        }
    }
}
