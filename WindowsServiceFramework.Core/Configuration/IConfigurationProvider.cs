﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
	public interface IConfigurationProvider
	{
		bool TryGet(string key, out string value);

		void Set(string key, string value);


		void Load();

		IEnumerable<string> GetChildKeys(IEnumerable<string> earlierKeys, string parentPath);
	}
}
