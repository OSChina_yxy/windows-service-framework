﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
	public interface IConfigurationBuilder
	{
		IDictionary<string, object> Properties { get; }

		IList<IConfigurationSource> Sources { get; }

		IConfigurationBuilder Add(IConfigurationSource source);

		IConfigurationRoot Build();
	}
}
