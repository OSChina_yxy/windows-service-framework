﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
    internal class JsonConfigurationFileParser
    {
        private JsonConfigurationFileParser()
        {
        }
        public static IDictionary<string, string> Parse(Stream input)
        {
            return new JsonConfigurationFileParser().ParseStream(input);
        }

        private IDictionary<string, string> ParseStream(Stream input)
        {
            this._data.Clear();
            using (StreamReader streamReader = new StreamReader(input))
            {
                JObject jObject = JObject.Parse(streamReader.ReadToEnd());
                foreach (JProperty prop in jObject.Properties())
                {
                    this.EnterContext(prop.Path);
                    this.VisitValue(prop);
                    this.ExitContext();
                }
            }
            return this._data;
        }

        private void VisitValue(JToken token)
        {
            if (!token.HasValues) return;

            string value = string.Empty;

            switch (token.Type)
            {
                case JTokenType.Object:
                    foreach (JProperty prop in token.ToObject<JObject>().Properties())
                    {
                        this.EnterContext(prop.Path);
                        this.VisitValue(prop);
                        this.ExitContext();
                    }
                    value = token.ToString();
                    break;
                case JTokenType.Property:
                    VisitValue(token.First);
                    value = token.First.ToString();
                    break;
                case JTokenType.Array:
                    int num = 0;
                    foreach (var tokenChild in token.ToArray())
                    {
                        this.EnterContext(num.ToString());
                        VisitValue(tokenChild);
                        this.ExitContext();
                        num++;
                    }
                    value = token.ToString();
                    break;
                default:
                    break;
            }

            string currentPath = this._currentPath;
            this._data[currentPath] = value;
        }

        private void EnterContext(string context)
        {
            this._context.Push(context);
            this._currentPath = ConfigurationPath.Combine(this._context.Reverse<string>());
        }

        private void ExitContext()
        {
            this._context.Pop();
            this._currentPath = ConfigurationPath.Combine(this._context.Reverse<string>());
        }

        private readonly IDictionary<string, string> _data = new SortedDictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        private readonly Stack<string> _context = new Stack<string>();

        private string _currentPath;
    }
}
