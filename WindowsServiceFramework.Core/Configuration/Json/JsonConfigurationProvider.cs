﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
    public class JsonConfigurationProvider : ConfigurationProvider
    {
        public JsonConfigurationSource Source { get; }
        public JsonConfigurationProvider(JsonConfigurationSource source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            this.Source = source;
        }

        public override void Load()
        {
            using (var stream = File.OpenRead(this.Source.Path))
            {
                this.Load(stream);
            }
        }

        public void Load(Stream stream)
        {
            base.Data = JsonConfigurationFileParser.Parse(stream);
        }
    }
}
