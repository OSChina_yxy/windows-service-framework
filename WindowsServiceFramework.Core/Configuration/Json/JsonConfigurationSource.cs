﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
	public class JsonConfigurationSource : IConfigurationSource
	{
		public string Path { get; set; }

		public IConfigurationProvider Build(IConfigurationBuilder builder)
		{
			return new JsonConfigurationProvider(this);
		}
	}
}
