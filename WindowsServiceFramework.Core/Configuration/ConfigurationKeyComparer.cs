﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
	public class ConfigurationKeyComparer : IComparer<string>
	{
		public static ConfigurationKeyComparer Instance { get; } = new ConfigurationKeyComparer();

		public int Compare(string x, string y)
		{
			string[] array = ((x != null) ? x.Split(ConfigurationKeyComparer._keyDelimiterArray, StringSplitOptions.RemoveEmptyEntries) : null) ?? new string[0];
			string[] array2 = ((y != null) ? y.Split(ConfigurationKeyComparer._keyDelimiterArray, StringSplitOptions.RemoveEmptyEntries) : null) ?? new string[0];
			for (int i = 0; i < Math.Min(array.Length, array2.Length); i++)
			{
				x = array[i];
				y = array2[i];
				int num = 0;
				int num2 = 0;
				bool flag = x != null && int.TryParse(x, out num);
				bool flag2 = y != null && int.TryParse(y, out num2);
				int num3;
				if (!flag && !flag2)
				{
					num3 = string.Compare(x, y, StringComparison.OrdinalIgnoreCase);
				}
				else if (flag && flag2)
				{
					num3 = num - num2;
				}
				else
				{
					num3 = (flag ? -1 : 1);
				}
				if (num3 != 0)
				{
					return num3;
				}
			}
			return array.Length - array2.Length;
		}

		private static readonly string[] _keyDelimiterArray = new string[]
		{
			ConfigurationPath.KeyDelimiter
		};
	}
}
