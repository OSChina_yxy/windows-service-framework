﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
	public abstract class ConfigurationProvider : IConfigurationProvider
	{
		protected ConfigurationProvider()
		{
			this.Data = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
		}

		protected IDictionary<string, string> Data { get; set; }

		public virtual bool TryGet(string key, out string value)
		{
			return this.Data.TryGetValue(key, out value);
		}
		public virtual void Set(string key, string value)
		{
			this.Data[key] = value;
		}

		public virtual void Load()
		{
		}
		public virtual IEnumerable<string> GetChildKeys(IEnumerable<string> earlierKeys, string parentPath)
		{
			string prefix = (parentPath == null) ? string.Empty : (parentPath + ConfigurationPath.KeyDelimiter);
			return (from kv in this.Data
					where kv.Key.StartsWith(prefix, StringComparison.OrdinalIgnoreCase)
					select ConfigurationProvider.Segment(kv.Key, prefix.Length)).Concat(earlierKeys).OrderBy((string k) => k, ConfigurationKeyComparer.Instance);
		}
		private static string Segment(string key, int prefixLength)
		{
			int num = key.IndexOf(ConfigurationPath.KeyDelimiter, prefixLength, StringComparison.OrdinalIgnoreCase);
			if (num >= 0)
			{
				return key.Substring(prefixLength, num - prefixLength);
			}
			return key.Substring(prefixLength);
		}
		public override string ToString()
		{
			return base.GetType().Name ?? "";
		}

	}
}
