﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsServiceFramework.Core.Configuration
{
	public interface IConfigurationRoot : IConfiguration
	{
		IEnumerable<IConfigurationProvider> Providers { get; }
	}
}
